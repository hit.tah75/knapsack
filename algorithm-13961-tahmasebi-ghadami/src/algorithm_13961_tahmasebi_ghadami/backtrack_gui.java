/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithm_13961_tahmasebi_ghadami;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.*;

/**
 *
 * @author mohammad
 */
public class backtrack_gui extends JPanel {

    public static class knapSack_node {

        int profit, weight;
        float bound;
        int height = 70, width = 60;
        int max_weight = 0;
        Point offset = new Point(0, 0);
        Point pos = new Point(0, 0);
        boolean best = false;
        String path = "";

        public knapSack_node(int p, int w, float b) {
            profit = p;
            weight = w;
            bound = b;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getPath() {
            return path;
        }

        public void setBest() {
            best = true;
        }

        public void setPoint(Point p) {
            pos.x = p.x;
            pos.y = p.y;
        }

        public Point getPoint() {
            return pos;
        }

        public void setBound(float b) {
            bound = b;
        }

        public void setMaxWeight(int w) {
            max_weight = w;
        }

        public float get_bound() {
            return bound;
        }

        public int get_weigth() {
            return weight;
        }

        public int get_profit() {
            return profit;
        }

        public Point get_upPoint() {
            return new Point(offset.x + width / 2, offset.y);
        }

        public Point get_downPoint() {
            return new Point(offset.x + width / 2, offset.y + height);
        }

        public void paint(Graphics2D g2d, int x, int y) {
            offset.x = x;
            offset.y = y;
            if (weight <= max_weight) {
                g2d.setPaint(new Color(138, 239, 127));
            } else {
                g2d.setPaint(new Color(239, 127, 127));

            }
            if (best) {
                g2d.setPaint(new Color(138, 250, 200));
            }
            g2d.fillRoundRect(x, y, width, height, 10, 10);
            g2d.setPaint(new Color(100, 100, 100));
            g2d.drawRoundRect(x, y, width, height, 10, 10);
            Font font = new Font("sansserif", Font.BOLD, 15);
            String text = String.valueOf(profit) + "$";
            FontMetrics metrics = g2d.getFontMetrics(font);
            int textx = x + ((width - metrics.stringWidth(text)) / 2);
            int texty = y + (3 * (height - metrics.getHeight()) / 24) + metrics.getAscent();
            g2d.setFont(font);
            g2d.setPaint(new Color(43, 71, 76));
            g2d.drawString(text, textx, texty);
            text = String.valueOf(weight);
            textx = x + ((width - metrics.stringWidth(text)) / 2);
            texty = y + ((height - metrics.getHeight()) / 2) + metrics.getAscent();
            g2d.drawString(text, textx, texty);
            try {
                File initialImage = new File("img/load_13.png");
                BufferedImage img = ImageIO.read(initialImage);
                int w = img.getWidth(null);
                int h = img.getHeight(null);

                g2d.drawImage(img, textx - 15, texty - 12, null);
            } catch (IOException e) {
                System.out.println("Exception occured :" + e.getMessage());
            }
            text = String.valueOf(bound) + "$";
            textx = x + ((width - metrics.stringWidth(text)) / 2);
            texty = y + (21 * (height - metrics.getHeight()) / 24) + metrics.getAscent();
            g2d.drawString(text, textx, texty);
        }
    }

    static final int WIDTH = 1000, HEIGHT = 700; // Size of our example
    static ArrayList<knapSack_node> nodes = new ArrayList<knapSack_node>();
    static boolean init = false;
    static ArrayList<JTextArea[]> boxes = new ArrayList<JTextArea[]>();
    static JButton DObtn = new JButton("DO");
    static JButton rstBtn = new JButton("Reset");
    static knapSack_backward kBW = new knapSack_backward();

    public float cost(int j) {
        return Integer.parseInt(boxes.get(j)[2].getText().trim()) / Integer.parseInt(boxes.get(j)[1].getText().trim());
    }

    public backtrack_gui() {
        JFrame frame = new JFrame("backtrack knapsack");
        frame.setSize(WIDTH, HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(this);
        frame.setLocationRelativeTo(this);
        frame.setLocation(frame.getLocation().x - 150, frame.getLocation().y);
        frame.setVisible(true);
        ////////////// setting frame
        JFrame frame2 = new JFrame("init");
        frame2.setSize(300, 530);
        frame2.setResizable(false);
        frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame2.setLocation(frame.getLocation().x + frame.getWidth(), frame.getLocation().y);
        final JLabel label = new JLabel();
        label.setText("    #            weight                  value");
        SpinnerModel value
                = new SpinnerNumberModel(3, //initial value  
                        1, //minimum value  
                        20, //maximum value  
                        1); //step  
        JSpinner spinner = new JSpinner(value);

        JPanel topPanel = new JPanel(new BorderLayout());
        JPanel ttopPanel = new JPanel(new BorderLayout());
        topPanel.add(label, BorderLayout.SOUTH);
        topPanel.add(ttopPanel, BorderLayout.NORTH);
        frame2.add(topPanel, BorderLayout.NORTH);
        JPanel bottomPanel = new JPanel(new BorderLayout());
        JPanel bbottomPanel = new JPanel(new BorderLayout());
        ttopPanel.add(new JLabel("max weight"), BorderLayout.WEST);
        JTextArea maxWeight = new JTextArea();
        ttopPanel.add(maxWeight);
        maxWeight.setSize(20, 100);
        bbottomPanel.add(DObtn, BorderLayout.EAST);
        bbottomPanel.add(rstBtn, BorderLayout.WEST);
        bbottomPanel.add(spinner);
        bottomPanel.add(bbottomPanel, BorderLayout.SOUTH);
        frame2.add(bottomPanel, BorderLayout.SOUTH);
        spinner.setBounds(100, 100, 50, 30);
        JPanel middlePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        frame2.add(middlePanel);
        for (int i = 0; i < (int) spinner.getValue(); i++) {
            JTextArea[] row = {new JTextArea(String.valueOf(i + 1)), new JTextArea("0"), new JTextArea("0")};
            row[0].setEditable(false);
            row[0].setEditable(false);
            row[0].setColumns(3);
            row[1].setColumns(10);
            row[2].setColumns(10);
            boxes.add(row);
            middlePanel.add(row[0]);
            middlePanel.add(row[1]);
            middlePanel.add(row[2]);
        }
        spinner.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                middlePanel.removeAll();
                middlePanel.revalidate();
                middlePanel.repaint();
                middlePanel.validate();
                ArrayList<JTextArea[]> copy = new ArrayList<JTextArea[]>();
                copy = (ArrayList<JTextArea[]>) boxes.clone();
                boxes.clear();
                for (int i = 0; i < (int) ((JSpinner) e.getSource()).getValue(); i++) {
                    JTextArea[] row = {new JTextArea(String.valueOf(i + 1)), new JTextArea("0"), new JTextArea("0")};
                    row[0].setEditable(false);
                    row[0].setColumns(3);
                    row[1].setColumns(10);
                    row[2].setColumns(10);
                    if (i < copy.size()) {
                        for (int j = 0; j < 3; j++) {
                            row[j] = copy.get(i)[j];
                        }
                    }
                    boxes.add(row);
                    middlePanel.add(row[0]);
                    middlePanel.add(row[1]);
                    middlePanel.add(row[2]);
                }
                frame2.revalidate();
                frame2.repaint();
            }
        });

        frame2.setVisible(true);
        rstBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                middlePanel.removeAll();
                middlePanel.revalidate();
                middlePanel.repaint();
                middlePanel.validate();
                boxes.clear();

                for (int i = 0; i < (int) spinner.getValue(); i++) {
                    JTextArea[] row = {new JTextArea(String.valueOf(i + 1)), new JTextArea("0"), new JTextArea("0")};
                    row[0].setEditable(false);
                    row[0].setEditable(false);
                    row[0].setColumns(3);
                    row[1].setColumns(10);
                    row[2].setColumns(10);
                    boxes.add(row);
                    middlePanel.add(row[0]);
                    middlePanel.add(row[1]);
                    middlePanel.add(row[2]);
                }
            }
        });
        DObtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean hasZero = false;
                for (int i = 0; i < boxes.size(); i++) {
                    if (Integer.parseInt(boxes.get(i)[1].getText().trim()) == 0 && !hasZero) {
                        hasZero = true;
                        JOptionPane.showMessageDialog(frame,
                                "Weights have to be non ZERO.",
                                "non weight error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
                if (!hasZero) {
                    nodes.clear();
                    for (int i = 0; i < boxes.size(); i++) {
                        for (int j = 1; j < (boxes.size() - i); j++) {
                            if (cost(j - 1) < cost(j)) {
                                JTextArea[] tmp = boxes.get(j - 1).clone();
                                boxes.set(j - 1, boxes.get(j).clone());
                                boxes.set(j, tmp);
                            }
                        }
                    }

                    middlePanel.removeAll();
                    middlePanel.revalidate();
                    middlePanel.repaint();
                    middlePanel.validate();

                    for (int i = 0; i < boxes.size(); i++) {
                        for (JTextArea row : boxes.get(i)) {
                            row.setEditable(false);
                            row.setText(row.getText().trim());
                            middlePanel.add(row);
                        }
                    }
                    frame2.revalidate();
                    frame2.repaint();
                    System.out.println("init ...");
                    ////////////////// sorted now doing algorithm
                    kBW.setMaxWeight(Integer.parseInt(maxWeight.getText()));
                    int v[] = new int[boxes.size()];
                    int w[] = new int[boxes.size()];
                    for (int i = 0; i < boxes.size(); i++) {
                        v[i] = Integer.parseInt(boxes.get(i)[2].getText());
                        w[i] = Integer.parseInt(boxes.get(i)[1].getText());
                    }
                    kBW.setboxes(v, w);
                    kBW.knapSack(0, 0, 0);
                    String bestNodePath = "0";
                    for (int i = 0; i < kBW.getBestPath().length; i++) {
                        boolean s = kBW.getBestPath()[i];
                        bestNodePath += s ? "1" : "0";
                        if (s) {
                            boxes.get(i)[0].setText(boxes.get(i)[0].getText() + " <>");
                        }
                        middlePanel.removeAll();
                        middlePanel.revalidate();
                        middlePanel.repaint();
                        middlePanel.validate();
                        for (int k = 0; k < boxes.size(); k++) {
                            JTextArea[] row = boxes.get(k);
                            middlePanel.add(row[0]);
                            middlePanel.add(row[1]);
                            middlePanel.add(row[2]);
                        }
                        frame2.revalidate();
                        frame2.repaint();
                    }
                    for (int i = 0; i < nodes.size(); i++) {
                        if (nodes.get(i).getPath().equals(bestNodePath)) {
                            nodes.get(i).setBest();
                        }
                    }
                    init = true;
                    repaint();
                }
            }
        });
    }

    public static void main(String args[]) {
        backtrack_gui main = new backtrack_gui();
        main.repaint();
    }

    public void drawNode2Node(Graphics2D g2d, knapSack_node n1, knapSack_node n2) {
        g2d.drawLine(n1.get_downPoint().x, n1.get_downPoint().y, n2.get_upPoint().x, n2.get_upPoint().y);
    }

    public void paint(Graphics g1) {

        g1.setColor(new Color(240, 240, 240));
        g1.fillRect(0, 0, this.WIDTH, this.HEIGHT);
        g1.setColor(new Color(30, 30, 30));

        g1.drawString("best profit: ", 20, 20);
        g1.drawString(String.valueOf(kBW.getBestProfit()), 25, 35);
        g1.drawString("best weight: ", 20, 50);
        g1.drawString(String.valueOf(kBW.getBestWeight()), 25, 65);
        Graphics2D g = (Graphics2D) g1;
        if (init) {
            for (int i = 0; i < nodes.size(); i++) {
                nodes.get(i).paint(g, 30 + (80 * nodes.get(i).getPoint().x) + (boxes.size() - nodes.get(i).getPoint().y) * 40, 30 + (100 * nodes.get(i).getPoint().y));
                //nodes.get(i).paint(g, 30 ,60);
            }
            for (int i = 0; i < nodes.size(); i++) {
                for (int j = 0; j < nodes.size(); j++) {
                    if (nodes.get(i).getPath().equals(nodes.get(j).getPath().substring(0, nodes.get(j).getPath().length() - 1))) {
                        drawNode2Node(g, nodes.get(i), nodes.get(j));
                    }
                }
            }
        }
    }

}
