package algorithm_13961_tahmasebi_ghadami;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class KnapSack_DP extends JPanel {

    static boolean init = false;
    static ArrayList<JTextArea[]> boxes = new ArrayList<JTextArea[]>();
    static JButton DObtn = new JButton("DO");
    static JButton rstBtn = new JButton("Reset");

    public float cost(int j) {
        return Integer.parseInt(boxes.get(j)[2].getText().trim()) / Integer.parseInt(boxes.get(j)[1].getText().trim());
    }

    public KnapSack_DP() {
        JFrame frame2 = new JFrame("knapSack DP");
        frame2.setSize(300, 550);
        frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame2.setLocationRelativeTo(this);
        frame2.setResizable(false);
               final JLabel label = new JLabel();
        label.setText("    #            weight                  value");
        SpinnerModel value
                = new SpinnerNumberModel(3, //initial value  
                        1, //minimum value  
                        20, //maximum value  
                        1); //step  
        JSpinner spinner = new JSpinner(value);

        JPanel topPanel = new JPanel(new BorderLayout());
        JPanel ttopPanel = new JPanel(new BorderLayout());
        topPanel.add(label, BorderLayout.SOUTH);
        topPanel.add(ttopPanel, BorderLayout.NORTH);
        frame2.add(topPanel, BorderLayout.NORTH);
        JPanel bottomPanel = new JPanel(new BorderLayout());
        JPanel bbottomPanel = new JPanel(new BorderLayout());
        ttopPanel.add(new JLabel("max weight"), BorderLayout.WEST);
        JTextArea maxWeight = new JTextArea();
        ttopPanel.add(maxWeight);
        maxWeight.setSize(20, 100);
        bbottomPanel.add(DObtn, BorderLayout.EAST);
        bbottomPanel.add(rstBtn, BorderLayout.WEST);
        bbottomPanel.add(spinner);
        JLabel ans = new JLabel("-------------------------------");
        bottomPanel.add(ans, BorderLayout.NORTH);
        bottomPanel.add(bbottomPanel, BorderLayout.SOUTH);
        frame2.add(bottomPanel, BorderLayout.SOUTH);
        spinner.setBounds(100, 100, 50, 30);
        JPanel middlePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        frame2.add(middlePanel);
        for (int i = 0; i < (int) spinner.getValue(); i++) {
            JTextArea[] row = {new JTextArea(String.valueOf(i + 1)), new JTextArea("0"), new JTextArea("0")};
            row[0].setEditable(false);
            row[0].setEditable(false);
            row[0].setColumns(3);
            row[1].setColumns(10);
            row[2].setColumns(10);
            boxes.add(row);
            middlePanel.add(row[0]);
            middlePanel.add(row[1]);
            middlePanel.add(row[2]);
        }
        spinner.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                middlePanel.removeAll();
                middlePanel.revalidate();
                middlePanel.repaint();
                middlePanel.validate();
                ArrayList<JTextArea[]> copy = new ArrayList<JTextArea[]>();
                copy = (ArrayList<JTextArea[]>) boxes.clone();
                boxes.clear();
                for (int i = 0; i < (int) ((JSpinner) e.getSource()).getValue(); i++) {
                    JTextArea[] row = {new JTextArea(String.valueOf(i + 1)), new JTextArea("0"), new JTextArea("0")};
                    row[0].setEditable(false);
                    row[0].setColumns(3);
                    row[1].setColumns(10);
                    row[2].setColumns(10);
                    if (i < copy.size()) {
                        for (int j = 0; j < 3; j++) {
                            row[j] = copy.get(i)[j];
                        }
                    }
                    boxes.add(row);
                    middlePanel.add(row[0]);
                    middlePanel.add(row[1]);
                    middlePanel.add(row[2]);
                }
                frame2.revalidate();
                frame2.repaint();
            }
        });

        frame2.setVisible(true);
        rstBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                middlePanel.removeAll();
                middlePanel.revalidate();
                middlePanel.repaint();
                middlePanel.validate();
                boxes.clear();

                for (int i = 0; i < (int) spinner.getValue(); i++) {
                    JTextArea[] row = {new JTextArea(String.valueOf(i + 1)), new JTextArea("0"), new JTextArea("0")};
                    row[0].setEditable(false);
                    row[0].setEditable(false);
                    row[0].setColumns(3);
                    row[1].setColumns(10);
                    row[2].setColumns(10);
                    boxes.add(row);
                    middlePanel.add(row[0]);
                    middlePanel.add(row[1]);
                    middlePanel.add(row[2]);
                }
            }
        });

        DObtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean hasZero = false;
                for (int i = 0; i < boxes.size(); i++) {
                    if (Integer.parseInt(boxes.get(i)[1].getText().trim()) == 0 && !hasZero) {
                        hasZero = true;
                        JOptionPane.showMessageDialog(frame2,
                                "Weights have to be non ZERO.",
                                "non weight error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
                if (!hasZero) {
                    for (int i = 0; i < boxes.size(); i++) {
                        for (int j = 1; j < (boxes.size() - i); j++) {
                            if (cost(j - 1) < cost(j)) {
                                JTextArea[] tmp = boxes.get(j - 1).clone();
                                boxes.set(j - 1, boxes.get(j).clone());
                                boxes.set(j, tmp);
                            }
                        }
                    }

                    middlePanel.removeAll();
                    middlePanel.revalidate();
                    middlePanel.repaint();
                    middlePanel.validate();

                    for (int i = 0; i < boxes.size(); i++) {
                        for(JTextArea row:boxes.get(i)){
                            row.setEditable(false);
                            row.setText(row.getText().trim());
                            middlePanel.add(row);
                        }
                    }
                    frame2.revalidate();
                    frame2.repaint();
                    System.out.println("init ...");
                    ////////////////// sorted now doing algorithm
                    int v[] = new int[boxes.size()];
                    int w[] = new int[boxes.size()];
                    for (int i = 0; i < boxes.size(); i++) {
                        v[i] = Integer.parseInt(boxes.get(i)[2].getText());
                        w[i] = Integer.parseInt(boxes.get(i)[1].getText());
                    }

                    String answer = String.valueOf(knapSack(Integer.parseInt(maxWeight.getText()), w, v, v.length));
                    ans.setFont(new Font("Consolas", Font.PLAIN, 20));
                    ans.setText("maximum profit is: " + answer);
                    init = true;
                    repaint();
                }
            }
        });
    }

    public static void main(String[] args) {
        new KnapSack_DP();
    }

    static int max(int a, int b) {
        return (a > b) ? a : b;
    }

    static int knapSack(int W, int wt[], int val[], int n) {
        int K[][] = new int[n + 1][W + 1];
        for (int col = 0; col <= W; col++) {
            K[0][col] = 0;
        }

        for (int row = 0; row <= n; row++) {
            K[row][0] = 0;
        }
        // Build table K[][] in bottom up manner
        for (int i = 0; i <= n; i++) {
            for (int w = 0; w <= W; w++) {
                if (i == 0 || w == 0) {
                    K[i][w] = 0;
                } else if (wt[i - 1] <= w) {
                    K[i][w] = max(val[i - 1] + K[i - 1][w - wt[i - 1]], K[i - 1][w]);
                } else {
                    K[i][w] = K[i - 1][w];
                }
            }
        }
        for (int[] rows : K) {
            for (int col : rows) {
                System.out.format("%5d", col);
            }
            System.out.println();
        }
        return K[n][W];
    }
}
