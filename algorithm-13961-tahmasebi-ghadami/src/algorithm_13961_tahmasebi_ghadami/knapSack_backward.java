/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithm_13961_tahmasebi_ghadami;

import algorithm_13961_tahmasebi_ghadami.backtrack_gui.knapSack_node;
import static algorithm_13961_tahmasebi_ghadami.backtrack_gui.nodes;
import java.awt.Point;

public class knapSack_backward {
    static int W ;
    static int n;
    static boolean include[];
    static boolean bestset[];
    static int maxprofit=0, numbest=0;
    static int p[];
    static int w[];
    static int bestWeight;
    public knapSack_backward(){
    }
    public void setMaxWeight(int w){
        W = w;
    }
    public int getBestProfit(){
        return maxprofit;
    }
     public int getBestWeight(){
        return bestWeight;
    }
    public boolean[] getBestPath(){
        boolean bestPath[] = new boolean[n];
        for (int i=0; i<n;i++){
            bestPath[i] = bestset[i+1];
        }
        return bestPath;
    }
    public void setboxes(int values[], int weights[]){
        n = values.length;
        p = new int[n+1];
        w = new int[n+1];
        for(int i=0; i<n; i++){
            p[i+1] = values[i];
            w[i+1] = weights[i];
        }
        include = new boolean[n+1];
        bestset = new boolean[n+1];
    }
    public static void main(String[] args){
        knapSack_backward kBW = new knapSack_backward();
        kBW.setMaxWeight(16);
        int v[] = {40,30,50,10}; 
        int w[] = {2,5,10,5};
        kBW.setboxes(v, w);
        kBW.knapSack(0,0,0);
        kBW.numbest = 0;
        System.out.print(kBW.getBestProfit());
        System.out.print("-->{");
            for (boolean s : kBW.getBestPath()){
                System.out.print(s);
                System.out.print(", ");
            }
            System.out.println("}");    
    }
    public static void knapSack(int i, int profit, int weight){
        knapSack_node t = new knapSack_node(profit,weight,0);
        String pos_x_tmp = "0";
        for (int k=1;k<i+1;k++){
            pos_x_tmp+= include[k] ? "1" : "0";
        }
        t.setPath(pos_x_tmp);
        int pos_x = (int)Math.pow(2,i) - Integer.parseInt(pos_x_tmp,2);
       /* System.out.print(i);
        System.out.print("  ");
        System.out.println((int)Math.pow(2,i) - Integer.parseInt(pos_x_tmp,2));*/
        t.setPoint(new Point((int)Math.pow(2,i) - Integer.parseInt(pos_x_tmp,2),i));
        t.setMaxWeight(W);
        nodes.add(t);
        if(weight <= W && profit > maxprofit){
            maxprofit = profit;
            bestset = include.clone();
            numbest = i;
            bestWeight = weight;
            /*System.out.print("{");
            for (boolean s : bestset){
                System.out.print(s);
                System.out.print(", ");
            }
            System.out.println("}");*/
        }
        if(promising(i, pos_x, profit,weight)){
            include[i+1] = true;
            knapSack(i+1, profit+p[i+1], weight+w[i+1]);
            include[i+1] = false;
            knapSack(i+1, profit, weight);
        }
    }
    public static boolean promising(int i, int pos_x, int profit, int weight){
    int j, k;
    int totWeight;
    float bound;
    
    if(weight >= W){
        //System.out.println("no bound");
        return false;
    }
    else{
        j = i + 1;
        bound = profit;
        totWeight = weight;
        while(j<=n && totWeight + w[j] <= W){
            totWeight = totWeight + w[j];
            bound = bound + p[j];
            j++;
        }
        k = j;
        if(k<= n)
            bound = bound + (W - totWeight) * p[k] / w[k];
        for(int p=0; p<nodes.size();p++)
            if(nodes.get(p).getPoint().y == i && nodes.get(p).getPoint().x == pos_x)
                nodes.get(p).setBound(bound);
        //System.out.println(bound);
        return bound > maxprofit;
    }
}
}
